-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2018 at 05:59 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kependudukan`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kelahiran`
--

CREATE TABLE IF NOT EXISTS `tabel_kelahiran` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `waktu_lahir` varchar(50) NOT NULL,
  `kelahiran_ke` varchar(20) NOT NULL,
  `berat` varchar(20) NOT NULL,
  `panjang` varchar(20) NOT NULL,
  `no_ktp_ibu` varchar(50) NOT NULL,
  `no_ktp_ayah` varchar(50) NOT NULL,
  `no_ktp_pelapor` varchar(50) NOT NULL,
  `no_ktp_saksi_1` varchar(50) NOT NULL,
  `no_ktp_saksi_2` varchar(50) NOT NULL,
  `no_ktp_kelurahan` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_kk`
--

CREATE TABLE IF NOT EXISTS `tabel_kk` (
  `no_kk` varchar(50) NOT NULL,
  `no_ktp_kepala_keluarga` varchar(50) NOT NULL,
  `no_ktp_kepala_dinas` varchar(50) NOT NULL,
  PRIMARY KEY (`no_kk`,`no_ktp_kepala_keluarga`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_ktm`
--

CREATE TABLE IF NOT EXISTS `tabel_ktm` (
  `no_ktp` varchar(50) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `no_ktp_kepala_desa` varchar(50) NOT NULL,
  `no_ktp_kepala_camat` varchar(50) NOT NULL,
  PRIMARY KEY (`no_surat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_ktp`
--

CREATE TABLE IF NOT EXISTS `tabel_ktp` (
  `no_ktp` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(30) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `kewarganegaraan` varchar(20) NOT NULL,
  `tempat_tanggal_lahir` varchar(100) NOT NULL,
  PRIMARY KEY (`no_ktp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_ktp_kk`
--

CREATE TABLE IF NOT EXISTS `table_ktp_kk` (
  `no_kk` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id` varchar(50) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `agama` varchar(30) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

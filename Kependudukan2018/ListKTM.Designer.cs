﻿namespace Kependudukan2018
{
    partial class ListKTM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListKtmGrid = new System.Windows.Forms.DataGridView();
            this.NomorKTM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nama = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.NoKtp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ListKtmGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // ListKtmGrid
            // 
            this.ListKtmGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ListKtmGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NomorKTM,
            this.Nama,
            this.NoKtp});
            this.ListKtmGrid.Location = new System.Drawing.Point(13, 29);
            this.ListKtmGrid.MultiSelect = false;
            this.ListKtmGrid.Name = "ListKtmGrid";
            this.ListKtmGrid.Size = new System.Drawing.Size(380, 191);
            this.ListKtmGrid.TabIndex = 0;
            // 
            // NomorKTM
            // 
            this.NomorKTM.HeaderText = "No";
            this.NomorKTM.Name = "NomorKTM";
            // 
            // Nama
            // 
            this.Nama.HeaderText = "Nama";
            this.Nama.Name = "Nama";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(317, 226);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "View";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // NoKtp
            // 
            this.NoKtp.HeaderText = "No Ktp";
            this.NoKtp.Name = "NoKtp";
            // 
            // ListKTM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 261);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ListKtmGrid);
            this.Name = "ListKTM";
            this.Text = "ListKTM";
            this.Load += new System.EventHandler(this.ListKTM_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ListKtmGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ListKtmGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomorKTM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nama;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoKtp;
    }
}
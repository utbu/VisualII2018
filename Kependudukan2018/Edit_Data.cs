﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Kependudukan2018
{

    public partial class Edit_Data : Form
    {
        private static MySqlConnection conn = new MySqlConnection("server =localhost; UID=root; Pwd=; database=kependudukan");
        public Edit_Data()
        {
            InitializeComponent();
        }

        private void buttonCari_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                if (textBoxCari != null && !string.IsNullOrWhiteSpace(textBoxCari.Text))
                {
                    cmd.CommandText = "Select * from tabel_ktp where no_ktp = " + textBoxCari.Text;
                }

                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        textBoxNIK.Text = (reader["no_ktp"].ToString());
                        textBoxNama.Text = (reader["nama"].ToString());
                        textBoxAlamat.Text = (reader["alamat"].ToString());
                        textBoxAgama.Text = (reader["agama"].ToString());
                        textBoxStatus.Text = (reader["status"].ToString());
                        textBoxPekerjaan.Text = (reader["pekerjaan"].ToString());
                        textBoxTTL.Text = (reader["tempat_tanggal_lahir"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed with error : " + ex.Message);
            }
            conn.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String jKel = "";
            bool isChecked = radioButtonLaki.Checked;
            if (isChecked)
            {
                jKel = radioButtonLaki.Text;
            }
            else
            {
                jKel = radioButtonPerempuan.Text;
            }

            String kwn = "";
            bool isCheckedKwn = radioButtonWni.Checked;
            if (isCheckedKwn)
            {
                kwn = radioButtonWni.Text;
            }
            else
            {
                kwn = radioButtonNonWni.Text;
            }

            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "UPDATE tabel_ktp SET no_ktp='"+textBoxNIK.Text+"', nama = '"+textBoxNama.Text+"', alamat = '"+textBoxAlamat.Text+"', jenis_kelamin = '"+jKel+"', agama = '"+textBoxAgama.Text+"', status = '"+textBoxStatus.Text+"', pekerjaan = '"+textBoxPekerjaan.Text+"', kewarganegaraan = '"+kwn+"', tempat_tanggal_lahir = '"+textBoxTTL.Text+"' where no_ktp = '"+textBoxCari.Text+"'";
                
                cmd.ExecuteNonQuery();

                labelResponse.Text = "Berhasil Edit data";
                labelResponse.BackColor = Color.GreenYellow;
            }
            catch (Exception ex)
            {
                labelResponse.Text = ex.Message;
                labelResponse.BackColor = Color.Pink;
            }
            conn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxNIK.Text = "";
            textBoxNama.Text = "";
            textBoxAlamat.Text = "";
            radioButtonLaki.Checked = false;
            radioButtonPerempuan.Checked = false;
            radioButtonWni.Checked = false;
            radioButtonNonWni.Checked = false;
            textBoxAgama.Text = "";
            textBoxStatus.Text = "";
            textBoxPekerjaan.Text = "";
            textBoxTTL.Text = "";
            labelResponse.Text = "Field dibersihkan";
            labelResponse.BackColor = Color.Aqua;
        }
    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kependudukan2018
{
    public partial class ViewKTM : Form
    {
        MySqlConnection conn = new MySqlConnection("Server=localhost;Database=kependudukan;Uid=root;Pwd=;");

        #region template
        string template = @"
Surat Keterangan TIdak Mampu
NOMOR : {0}

Yang bertanda tangan dibawah ini Kepala Desa SUKAHARJA Kecamatan SUKASUKA. Kabupaten BANDUNG
Dengan ini menerangkan bahwa :

Nama : {1}.
Tempat/Tanggal Lahir : {2}.
Jenis Kelamin : {3}
Nama Orang Tua : {4}.
Alamat Orang Tua : {5}

 

Yang bersangkutan adalah benar-benar warga Desa SUKAHARJA. yang berasal dari keluarga kurang mampu.

Demikian surat keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.

 

BANDUNG, 30 Agustus 2016

Kepala Desa SUKAHARJA

 

{6}.
";
        #endregion template
        private string noKtm = "";
        private string nama = "";
        private string tempatTanggalLahir = "";
        private string jenisKelamin = "";
        private string namaOrtu = "";
        private string alamatOrtu = "";
        private string nama_kades = "";
        private bool isExist = true;
        public ViewKTM()
        {
            isExist = false;
            InitializeComponent();
        }
        public ViewKTM(string noKtm)
        {
            isExist = true;
            this.noKtm = noKtm;
            InitializeComponent();
        }
        public ViewKTM(string noKtm,string nama, string tempatTanggalLahir, string jenisKelamin, string namaOrtu, string alamatOrtu, string nama_kades)
        {
            isExist = false;
            this.nama = nama;
            this.noKtm = noKtm;
            this.tempatTanggalLahir = tempatTanggalLahir;
            this.jenisKelamin= jenisKelamin;
            this.namaOrtu= namaOrtu;
            this.alamatOrtu= alamatOrtu;
            this.nama_kades= nama_kades;
            InitializeComponent();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ViewKTM_Load(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                if (isExist)
                {

                    string query = @"SELECT ktm.no_surat,ktp.nama,ktp.tempat_tanggal_lahir,ktp.jenis_kelamin,
ktp_ortu.nama AS nama_ortu,ktp_ortu.alamat AS alamat_ortu,ktp_desa.nama AS nama_kades
FROM tabel_ktm ktm
LEFT JOIN tabel_ktp ktp on ktm.no_ktp=ktp.no_ktp
LEFT JOIN tabel_ktp ktp_ortu on ktm.no_ktp_kepala_camat=ktp_ortu.no_ktp
LEFT JOIN tabel_ktp ktp_desa on ktm.no_ktp_kepala_desa=ktp_desa.no_ktp
WHERE ktm.no_surat='{0}';
";
                    query = string.Format(query, noKtm);
                    var sql = new MySqlCommand(query, conn);
                    MySqlDataReader read = sql.ExecuteReader();
                    bool isFound = false;
                    while (read.Read())
                    {
                        nama = read.GetString("nama") ?? "";
                        tempatTanggalLahir = read.GetString("tempat_tanggal_lahir");
                        jenisKelamin = read.GetString("jenis_kelamin");
                        namaOrtu = read.GetString("nama_ortu");
                        alamatOrtu = read.GetString("alamat_ortu");
                        nama_kades = read.GetString("nama_kades");

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
            finally
            {
                conn.Close();
                var surat = string.Format(template, noKtm, nama, tempatTanggalLahir,
                        jenisKelamin, namaOrtu, alamatOrtu, nama_kades);
                label1.Text = surat;
            }
        }
    }
}

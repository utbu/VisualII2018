﻿namespace Kependudukan2018
{
    partial class Input_Data_Baru
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelKel = new System.Windows.Forms.Panel();
            this.radioButtonPerempuan = new System.Windows.Forms.RadioButton();
            this.radioButtonLaki = new System.Windows.Forms.RadioButton();
            this.textBoxTTL = new System.Windows.Forms.TextBox();
            this.textBoxPekerjaan = new System.Windows.Forms.TextBox();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.textBoxAgama = new System.Windows.Forms.TextBox();
            this.textBoxAlamat = new System.Windows.Forms.TextBox();
            this.textBoxNama = new System.Windows.Forms.TextBox();
            this.textBoxNIK = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panelKwn = new System.Windows.Forms.Panel();
            this.radioButtonNonWni = new System.Windows.Forms.RadioButton();
            this.radioButtonWni = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.labelResponse = new System.Windows.Forms.Label();
            this.panelKel.SuspendLayout();
            this.panelKwn.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelKel
            // 
            this.panelKel.Controls.Add(this.radioButtonPerempuan);
            this.panelKel.Controls.Add(this.radioButtonLaki);
            this.panelKel.Location = new System.Drawing.Point(242, 167);
            this.panelKel.Margin = new System.Windows.Forms.Padding(4);
            this.panelKel.Name = "panelKel";
            this.panelKel.Size = new System.Drawing.Size(150, 57);
            this.panelKel.TabIndex = 17;
            // 
            // radioButtonPerempuan
            // 
            this.radioButtonPerempuan.AutoSize = true;
            this.radioButtonPerempuan.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonPerempuan.Location = new System.Drawing.Point(4, 31);
            this.radioButtonPerempuan.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonPerempuan.Name = "radioButtonPerempuan";
            this.radioButtonPerempuan.Size = new System.Drawing.Size(84, 19);
            this.radioButtonPerempuan.TabIndex = 9;
            this.radioButtonPerempuan.TabStop = true;
            this.radioButtonPerempuan.Text = "Perempuan";
            this.radioButtonPerempuan.UseVisualStyleBackColor = true;
            // 
            // radioButtonLaki
            // 
            this.radioButtonLaki.AutoSize = true;
            this.radioButtonLaki.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonLaki.Location = new System.Drawing.Point(4, 4);
            this.radioButtonLaki.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonLaki.Name = "radioButtonLaki";
            this.radioButtonLaki.Size = new System.Drawing.Size(69, 19);
            this.radioButtonLaki.TabIndex = 0;
            this.radioButtonLaki.TabStop = true;
            this.radioButtonLaki.Text = "Laki-laki";
            this.radioButtonLaki.UseVisualStyleBackColor = true;
            // 
            // textBoxTTL
            // 
            this.textBoxTTL.Location = new System.Drawing.Point(242, 389);
            this.textBoxTTL.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTTL.Name = "textBoxTTL";
            this.textBoxTTL.Size = new System.Drawing.Size(148, 26);
            this.textBoxTTL.TabIndex = 15;
            // 
            // textBoxPekerjaan
            // 
            this.textBoxPekerjaan.Location = new System.Drawing.Point(242, 300);
            this.textBoxPekerjaan.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPekerjaan.Name = "textBoxPekerjaan";
            this.textBoxPekerjaan.Size = new System.Drawing.Size(148, 26);
            this.textBoxPekerjaan.TabIndex = 14;
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.Location = new System.Drawing.Point(242, 266);
            this.textBoxStatus.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.Size = new System.Drawing.Size(148, 26);
            this.textBoxStatus.TabIndex = 13;
            // 
            // textBoxAgama
            // 
            this.textBoxAgama.Location = new System.Drawing.Point(242, 232);
            this.textBoxAgama.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAgama.Name = "textBoxAgama";
            this.textBoxAgama.Size = new System.Drawing.Size(148, 26);
            this.textBoxAgama.TabIndex = 12;
            // 
            // textBoxAlamat
            // 
            this.textBoxAlamat.Location = new System.Drawing.Point(242, 133);
            this.textBoxAlamat.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAlamat.Name = "textBoxAlamat";
            this.textBoxAlamat.Size = new System.Drawing.Size(148, 26);
            this.textBoxAlamat.TabIndex = 11;
            // 
            // textBoxNama
            // 
            this.textBoxNama.Location = new System.Drawing.Point(242, 99);
            this.textBoxNama.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxNama.Name = "textBoxNama";
            this.textBoxNama.Size = new System.Drawing.Size(148, 26);
            this.textBoxNama.TabIndex = 10;
            // 
            // textBoxNIK
            // 
            this.textBoxNIK.Location = new System.Drawing.Point(242, 65);
            this.textBoxNIK.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxNIK.Name = "textBoxNIK";
            this.textBoxNIK.Size = new System.Drawing.Size(148, 26);
            this.textBoxNIK.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Rockwell", 16F);
            this.label1.Location = new System.Drawing.Point(121, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 25);
            this.label1.TabIndex = 18;
            this.label1.Text = ".:: Input Data Baru ::.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(46, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 19);
            this.label2.TabIndex = 19;
            this.label2.Text = "NIK";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(46, 99);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 19);
            this.label3.TabIndex = 20;
            this.label3.Text = "Nama Lengkap";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(46, 133);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 19);
            this.label4.TabIndex = 21;
            this.label4.Text = "Alamat";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(46, 171);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 19);
            this.label5.TabIndex = 22;
            this.label5.Text = "Jenis Kelamin";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(46, 232);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 19);
            this.label6.TabIndex = 23;
            this.label6.Text = "Agama";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(46, 266);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 19);
            this.label7.TabIndex = 24;
            this.label7.Text = "Status";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(46, 300);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 19);
            this.label8.TabIndex = 25;
            this.label8.Text = "Pekerjaan";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(46, 338);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 19);
            this.label9.TabIndex = 26;
            this.label9.Text = "Kewarganegaraan";
            // 
            // panelKwn
            // 
            this.panelKwn.Controls.Add(this.radioButtonNonWni);
            this.panelKwn.Controls.Add(this.radioButtonWni);
            this.panelKwn.Location = new System.Drawing.Point(242, 334);
            this.panelKwn.Margin = new System.Windows.Forms.Padding(4);
            this.panelKwn.Name = "panelKwn";
            this.panelKwn.Size = new System.Drawing.Size(150, 47);
            this.panelKwn.TabIndex = 18;
            // 
            // radioButtonNonWni
            // 
            this.radioButtonNonWni.AutoSize = true;
            this.radioButtonNonWni.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonNonWni.Location = new System.Drawing.Point(4, 26);
            this.radioButtonNonWni.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonNonWni.Name = "radioButtonNonWni";
            this.radioButtonNonWni.Size = new System.Drawing.Size(57, 19);
            this.radioButtonNonWni.TabIndex = 9;
            this.radioButtonNonWni.TabStop = true;
            this.radioButtonNonWni.Text = "WNA";
            this.radioButtonNonWni.UseVisualStyleBackColor = true;
            // 
            // radioButtonWni
            // 
            this.radioButtonWni.AutoSize = true;
            this.radioButtonWni.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonWni.Location = new System.Drawing.Point(4, 4);
            this.radioButtonWni.Margin = new System.Windows.Forms.Padding(4);
            this.radioButtonWni.Name = "radioButtonWni";
            this.radioButtonWni.Size = new System.Drawing.Size(51, 19);
            this.radioButtonWni.TabIndex = 0;
            this.radioButtonWni.TabStop = true;
            this.radioButtonWni.Text = "WNI";
            this.radioButtonWni.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(46, 389);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 19);
            this.label10.TabIndex = 27;
            this.label10.Text = "Tempat Tanggal Lahir";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(59, 432);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(164, 33);
            this.button1.TabIndex = 28;
            this.button1.Text = "Bersihkan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(222, 432);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(164, 33);
            this.button2.TabIndex = 29;
            this.button2.Text = "Simpan";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(433, 19);
            this.label11.TabIndex = 30;
            this.label11.Text = "_____________________________________________________";
            // 
            // labelResponse
            // 
            this.labelResponse.AutoSize = true;
            this.labelResponse.Location = new System.Drawing.Point(48, 473);
            this.labelResponse.Name = "labelResponse";
            this.labelResponse.Size = new System.Drawing.Size(68, 19);
            this.labelResponse.TabIndex = 31;
            this.labelResponse.Text = "Response";
            // 
            // Input_Data_Baru
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 529);
            this.Controls.Add(this.labelResponse);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.panelKwn);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelKel);
            this.Controls.Add(this.textBoxTTL);
            this.Controls.Add(this.textBoxPekerjaan);
            this.Controls.Add(this.textBoxStatus);
            this.Controls.Add(this.textBoxAgama);
            this.Controls.Add(this.textBoxAlamat);
            this.Controls.Add(this.textBoxNama);
            this.Controls.Add(this.textBoxNIK);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Input_Data_Baru";
            this.Text = "Form Input Data Baru";
            this.panelKel.ResumeLayout(false);
            this.panelKel.PerformLayout();
            this.panelKwn.ResumeLayout(false);
            this.panelKwn.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNIK;
        private System.Windows.Forms.TextBox textBoxNama;
        private System.Windows.Forms.TextBox textBoxAlamat;
        private System.Windows.Forms.TextBox textBoxAgama;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.TextBox textBoxPekerjaan;
        private System.Windows.Forms.TextBox textBoxTTL;
        private System.Windows.Forms.RadioButton radioButtonLaki;
        private System.Windows.Forms.RadioButton radioButtonPerempuan;
        private System.Windows.Forms.Panel panelKel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panelKwn;
        private System.Windows.Forms.RadioButton radioButtonNonWni;
        private System.Windows.Forms.RadioButton radioButtonWni;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelResponse;
    }
}
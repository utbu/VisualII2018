﻿namespace Kependudukan2018
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pendudukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataPendudukToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tutupDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tambahDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hapusDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kelahiranToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pembuatanAkteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tampilkanDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tutupDataAkteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kTMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataKTMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pembuatanKTMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pendudukToolStripMenuItem,
            this.kelahiranToolStripMenuItem,
            this.kTMToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1146, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pendudukToolStripMenuItem
            // 
            this.pendudukToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataPendudukToolStripMenuItem,
            this.tutupDataToolStripMenuItem,
            this.tambahDataToolStripMenuItem,
            this.editDataToolStripMenuItem,
            this.hapusDataToolStripMenuItem});
            this.pendudukToolStripMenuItem.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.pendudukToolStripMenuItem.Name = "pendudukToolStripMenuItem";
            this.pendudukToolStripMenuItem.Size = new System.Drawing.Size(78, 21);
            this.pendudukToolStripMenuItem.Text = "Penduduk";
            // 
            // dataPendudukToolStripMenuItem
            // 
            this.dataPendudukToolStripMenuItem.Name = "dataPendudukToolStripMenuItem";
            this.dataPendudukToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.dataPendudukToolStripMenuItem.Text = "Data Penduduk";
            this.dataPendudukToolStripMenuItem.Click += new System.EventHandler(this.dataPendudukToolStripMenuItem_Click);
            // 
            // tutupDataToolStripMenuItem
            // 
            this.tutupDataToolStripMenuItem.Name = "tutupDataToolStripMenuItem";
            this.tutupDataToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.tutupDataToolStripMenuItem.Text = "Tutup Data";
            this.tutupDataToolStripMenuItem.Click += new System.EventHandler(this.tutupDataToolStripMenuItem_Click);
            // 
            // tambahDataToolStripMenuItem
            // 
            this.tambahDataToolStripMenuItem.Name = "tambahDataToolStripMenuItem";
            this.tambahDataToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.tambahDataToolStripMenuItem.Text = "Tambah Data";
            this.tambahDataToolStripMenuItem.Click += new System.EventHandler(this.tambahDataToolStripMenuItem_Click);
            // 
            // editDataToolStripMenuItem
            // 
            this.editDataToolStripMenuItem.Name = "editDataToolStripMenuItem";
            this.editDataToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.editDataToolStripMenuItem.Text = "Edit Data";
            this.editDataToolStripMenuItem.Click += new System.EventHandler(this.editDataToolStripMenuItem_Click);
            // 
            // hapusDataToolStripMenuItem
            // 
            this.hapusDataToolStripMenuItem.Name = "hapusDataToolStripMenuItem";
            this.hapusDataToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.hapusDataToolStripMenuItem.Text = "Hapus Data";
            this.hapusDataToolStripMenuItem.Click += new System.EventHandler(this.hapusDataToolStripMenuItem_Click);
            // 
            // kelahiranToolStripMenuItem
            // 
            this.kelahiranToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pembuatanAkteToolStripMenuItem,
            this.tampilkanDataToolStripMenuItem,
            this.tutupDataAkteToolStripMenuItem});
            this.kelahiranToolStripMenuItem.Font = new System.Drawing.Font("Times New Roman", 11F);
            this.kelahiranToolStripMenuItem.Name = "kelahiranToolStripMenuItem";
            this.kelahiranToolStripMenuItem.Size = new System.Drawing.Size(77, 21);
            this.kelahiranToolStripMenuItem.Text = "Kelahiran";
            // 
            // pembuatanAkteToolStripMenuItem
            // 
            this.pembuatanAkteToolStripMenuItem.Name = "pembuatanAkteToolStripMenuItem";
            this.pembuatanAkteToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.pembuatanAkteToolStripMenuItem.Text = "Pembuatan Akte";
            this.pembuatanAkteToolStripMenuItem.Click += new System.EventHandler(this.pembuatanAkteToolStripMenuItem_Click);
            // 
            // tampilkanDataToolStripMenuItem
            // 
            this.tampilkanDataToolStripMenuItem.Name = "tampilkanDataToolStripMenuItem";
            this.tampilkanDataToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.tampilkanDataToolStripMenuItem.Text = "Data Akte";
            this.tampilkanDataToolStripMenuItem.Click += new System.EventHandler(this.tampilkanDataToolStripMenuItem_Click);
            // 
            // tutupDataAkteToolStripMenuItem
            // 
            this.tutupDataAkteToolStripMenuItem.Name = "tutupDataAkteToolStripMenuItem";
            this.tutupDataAkteToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.tutupDataAkteToolStripMenuItem.Text = "Tutup Data Akte";
            this.tutupDataAkteToolStripMenuItem.Click += new System.EventHandler(this.tutupDataAkteToolStripMenuItem_Click);
            // 
            // kTMToolStripMenuItem
            // 
            this.kTMToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataKTMToolStripMenuItem,
            this.pembuatanKTMToolStripMenuItem});
            this.kTMToolStripMenuItem.Name = "kTMToolStripMenuItem";
            this.kTMToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.kTMToolStripMenuItem.Text = "KTM";
            // 
            // dataKTMToolStripMenuItem
            // 
            this.dataKTMToolStripMenuItem.Name = "dataKTMToolStripMenuItem";
            this.dataKTMToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.dataKTMToolStripMenuItem.Text = "Data KTM";
            this.dataKTMToolStripMenuItem.Click += new System.EventHandler(this.dataKTMToolStripMenuItem_Click);
            // 
            // pembuatanKTMToolStripMenuItem
            // 
            this.pembuatanKTMToolStripMenuItem.Name = "pembuatanKTMToolStripMenuItem";
            this.pembuatanKTMToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.pembuatanKTMToolStripMenuItem.Text = "Pembuatan KTM";
            this.pembuatanKTMToolStripMenuItem.Click += new System.EventHandler(this.pembuatanKTMToolStripMenuItem_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9});
            this.listView1.Location = new System.Drawing.Point(12, 40);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(128, 27);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nomor KTP";
            this.columnHeader1.Width = -2;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nama";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Alamat";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Jenis Kelamin";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Agama";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Status";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Pekerjaan";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Kewarganegaraan";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Tempat Tanggal Lahir";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(353, 276);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(415, 154);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Rosewood Std Regular", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.ForestGreen;
            this.label1.Location = new System.Drawing.Point(774, 276);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 43);
            this.label1.TabIndex = 7;
            this.label1.Text = "KAMPUNG";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Rosewood Std Regular", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.ForestGreen;
            this.label2.Location = new System.Drawing.Point(774, 319);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 43);
            this.label2.TabIndex = 8;
            this.label2.Text = "RUMPUT";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(918, 322);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(92, 108);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1146, 482);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Pelayanan Terpadu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pendudukToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataPendudukToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tambahDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hapusDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kelahiranToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pembuatanAkteToolStripMenuItem;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ToolStripMenuItem tutupDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tampilkanDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tutupDataAkteToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem kTMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataKTMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pembuatanKTMToolStripMenuItem;
    }
}


﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kependudukan2018
{
    public partial class ListKTM : Form
    {
        MySqlConnection conn = new MySqlConnection("Server=localhost;Database=kependudukan;Uid=root;Pwd=;");
        public ListKTM()
        {
            InitializeComponent();
        }

        private void ListKTM_Load(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                string query = "SELECT ktm.no_surat, ktm.no_ktp,ktp.nama FROM tabel_ktm ktm LEFT JOIN tabel_ktp ktp ON ktm.no_ktp = ktp.no_ktp";
                var sql = new MySqlCommand(query, conn);
                MySqlDataReader read = sql.ExecuteReader();
                bool isFound = false;
                while (read.Read())
                {
                    string noKTM = read.GetString("no_surat") ?? "";
                    string nama = read.GetString("nama") ?? "";
                    string noKtp = read.GetString("no_ktp");
                    ListKtmGrid.Rows.Add(noKTM, nama, noKtp);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
            finally
            {
                conn.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var row = ListKtmGrid.CurrentRow;
            if (row != null)
            {
                var noKtm = row.Cells["NomorKTM"].Value as string ?? "";
                ViewKTM form = new ViewKTM(noKtm);
                form.Show();
            }

        }
    }
}

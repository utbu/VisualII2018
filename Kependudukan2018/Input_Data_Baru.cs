﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Kependudukan2018
{
    public partial class Input_Data_Baru : Form
    {

        private static MySqlConnection conn = new MySqlConnection("server =localhost; UID=root; Pwd=; database=kependudukan");
        public Input_Data_Baru()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String jKel = "";
            bool isChecked = radioButtonLaki.Checked;
            if (isChecked)
            {
                jKel = radioButtonLaki.Text;
            } else
            {
                jKel = radioButtonPerempuan.Text;
            }

            String kwn = "";
            bool isCheckedKwn = radioButtonWni.Checked;
            if (isCheckedKwn)
            {
                kwn = radioButtonWni.Text;
            } else
            {
                kwn = radioButtonNonWni.Text;
            }

            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                if (textBoxNIK != null && !string.IsNullOrWhiteSpace(textBoxNIK.Text))
                {
                    cmd.CommandText = "insert into tabel_ktp (no_ktp, nama, alamat, jenis_kelamin, agama, status, pekerjaan, kewarganegaraan, tempat_tanggal_lahir) values ('" + textBoxNIK.Text + "', '" + textBoxNama.Text + "', '" + textBoxAlamat.Text + "', '" + jKel + "', '" + textBoxAgama.Text + "', '" + textBoxStatus.Text + "', '" + textBoxPekerjaan.Text + "', '" + kwn + "', '" + textBoxTTL.Text + "')";
                }
                cmd.ExecuteNonQuery();

                labelResponse.Text = "Berhasil input data baru";
                labelResponse.BackColor = Color.GreenYellow;
            }
            catch (Exception ex)
            {
                labelResponse.Text = ex.Message;
                labelResponse.BackColor = Color.Pink;
            }
            conn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBoxNIK.Text = "";
            textBoxNama.Text = "";
            textBoxAlamat.Text = "";
            radioButtonLaki.Checked = false;
            radioButtonPerempuan.Checked = false;
            radioButtonWni.Checked = false;
            radioButtonNonWni.Checked = false;
            textBoxAgama.Text = "";
            textBoxStatus.Text = "";
            textBoxPekerjaan.Text = "";
            textBoxTTL.Text = "";
            labelResponse.Text = "Field dibersihkan";
            labelResponse.BackColor = Color.Aqua;
        }
    }
}
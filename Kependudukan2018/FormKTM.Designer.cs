﻿namespace Kependudukan2018
{
    partial class FormKTM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ktp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ktpOrtu = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ktpDesa = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonPreview = new System.Windows.Forms.Button();
            this.noSurat = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ktp
            // 
            this.ktp.Location = new System.Drawing.Point(176, 106);
            this.ktp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ktp.Name = "ktp";
            this.ktp.Size = new System.Drawing.Size(234, 26);
            this.ktp.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 106);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "No KTP";
            // 
            // ktpOrtu
            // 
            this.ktpOrtu.Location = new System.Drawing.Point(176, 140);
            this.ktpOrtu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ktpOrtu.Name = "ktpOrtu";
            this.ktpOrtu.Size = new System.Drawing.Size(233, 26);
            this.ktpOrtu.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 140);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "No KTP Orang Tua";
            // 
            // ktpDesa
            // 
            this.ktpDesa.Location = new System.Drawing.Point(177, 174);
            this.ktpDesa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ktpDesa.Name = "ktpDesa";
            this.ktpDesa.Size = new System.Drawing.Size(233, 26);
            this.ktpDesa.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 174);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "No KTP Kepala Desa";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(297, 208);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(112, 34);
            this.buttonSave.TabIndex = 2;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonPreview
            // 
            this.buttonPreview.Location = new System.Drawing.Point(177, 208);
            this.buttonPreview.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonPreview.Name = "buttonPreview";
            this.buttonPreview.Size = new System.Drawing.Size(112, 34);
            this.buttonPreview.TabIndex = 2;
            this.buttonPreview.Text = "Preview";
            this.buttonPreview.UseVisualStyleBackColor = true;
            this.buttonPreview.Click += new System.EventHandler(this.buttonPreview_Click);
            // 
            // noSurat
            // 
            this.noSurat.Location = new System.Drawing.Point(176, 72);
            this.noSurat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.noSurat.Name = "noSurat";
            this.noSurat.Size = new System.Drawing.Size(233, 26);
            this.noSurat.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 19);
            this.label4.TabIndex = 1;
            this.label4.Text = "Nomor Surat";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Rockwell", 16F);
            this.label5.Location = new System.Drawing.Point(130, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 25);
            this.label5.TabIndex = 54;
            this.label5.Text = ".:: FORM KTM ::.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(417, 19);
            this.label11.TabIndex = 55;
            this.label11.Text = "___________________________________________________";
            // 
            // FormKTM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 264);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.buttonPreview);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ktpDesa);
            this.Controls.Add(this.noSurat);
            this.Controls.Add(this.ktpOrtu);
            this.Controls.Add(this.ktp);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormKTM";
            this.Text = "FormKTM";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ktp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ktpOrtu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ktpDesa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonPreview;
        private System.Windows.Forms.TextBox noSurat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
    }
}
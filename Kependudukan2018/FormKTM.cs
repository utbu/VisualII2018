﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kependudukan2018
{
    public partial class FormKTM : Form
    {
        MySqlConnection conn = new MySqlConnection("Server=localhost;Database=kependudukan;Uid=root;Pwd=;");

        public FormKTM()
        {
            InitializeComponent();
        }

        private void buttonPreview_Click(object sender, EventArgs e)
        {
            string noKtp = ktp.Text;
            string nama = "";
            string tempat_tanggal_ahir = "";
            string alamat = "";
            string jenis_kelamin = "";
            GetKtpData(noKtp, out nama, out tempat_tanggal_ahir, out alamat, out jenis_kelamin);

            string noKtpOrtu = ktpOrtu.Text;
            string namaOrtu = "";
            string tempat_tanggal_ahirOrtu = "";
            string alamatOrtu = "";
            string jenis_kelaminOrtu = "";
            GetKtpData(noKtpOrtu, out namaOrtu, out tempat_tanggal_ahirOrtu, out alamatOrtu, out jenis_kelaminOrtu);

            string noKtpDesa = ktpDesa.Text;
            string namaDesa = "";
            string tempat_tanggal_ahirDesa = "";
            string alamatDesa = "";
            string jenis_kelaminDesa = "";
            GetKtpData(noKtpDesa, out namaDesa, out tempat_tanggal_ahirDesa, out alamatDesa, out jenis_kelaminDesa);

            ViewKTM form = new ViewKTM(noSurat.Text,nama,tempat_tanggal_ahir,jenis_kelamin,
                namaOrtu,alamatOrtu,namaDesa);
            form.Show();
        }
        private void GetKtpData(string noKtp,out string nama,out string tempat_tanggal_ahir,
            out string alamat,out string jenis_kelamin)
        {
            nama =  "";
            tempat_tanggal_ahir = "";
            alamat = "";
            jenis_kelamin = "";
            try
            {
                conn.Open();
                string template = "Select * FROM tabel_ktp WHERE no_ktp='{0}'";
                string query = string.Format(template, noKtp);
                var sql = new MySqlCommand(query, conn);
                MySqlDataReader read = sql.ExecuteReader();
                while (read.Read())
                {
                    nama = read.GetString("nama") ?? "";
                    tempat_tanggal_ahir = read.GetString("tempat_tanggal_lahir");
                    jenis_kelamin = read.GetString("jenis_kelamin");
                    alamat = read.GetString("alamat");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
            finally
            {
                conn.Close();
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                string template = @"
INSERT INTO 
`tabel_ktm`(`no_ktp`, `no_surat`, `no_ktp_kepala_desa`, `no_ktp_kepala_camat`) 
VALUES ('{0}','{1}','{2}','{3}')";
                string query = string.Format(template,ktp.Text,noSurat.Text,ktpDesa.Text,ktpOrtu.Text);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                cmd.ExecuteNonQuery();
                MessageBox.Show("Tersimpan");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}

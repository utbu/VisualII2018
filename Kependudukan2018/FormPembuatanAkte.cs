﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Reflection;
using System.IO;

namespace Kependudukan2018
{
    public partial class FormPembuatanAkte : Form
    {
        private static MySqlConnection conn = new MySqlConnection("server =localhost; UID=root; Pwd=; database=kependudukan");
        String noKtpIbu, noKtpAyah, noKtpPelapor, noKtpSaksi1, noKtpSaksi2, noKtpKelurahan, detailLahir;

        public FormPembuatanAkte()
        {
            
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                if (textBox1 != null && !string.IsNullOrWhiteSpace(textBox1.Text))
                {
                    detailLahir = dateTimePicker1.Text;
                    cmd.CommandText = "insert into tabel_kelahiran (id, nama, jenis_kelamin, tempat_lahir, waktu_lahir, kelahiran_ke, berat, panjang, no_ktp_ibu, no_ktp_ayah, no_ktp_pelapor, no_ktp_saksi_1, no_ktp_saksi_2, no_ktp_kelurahan) values ('" + textBox14.Text + "', '" + textBox1.Text + "', '" + textBox2.Text + "', '" + textBox3.Text + "', '" + detailLahir + "', '" + textBox5.Text + "', '" + textBox6.Text + "', '" + textBox7.Text + "', '" + noKtpIbu + "', '" + noKtpAyah + "', '" + noKtpPelapor + "', '" + noKtpSaksi1 + "', '" + noKtpSaksi2 + "', '" + noKtpKelurahan + "')";
                }
                cmd.ExecuteNonQuery();

                labelResponse.Text = "Berhasil input data baru";
                labelResponse.BackColor = Color.GreenYellow;

                object fileName = Path.Combine(System.Windows.Forms.Application.StartupPath, "documents.docx");
                Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application { Visible = true };
                Microsoft.Office.Interop.Word.Document aDoc = wordApp.Documents.Open(fileName, ReadOnly: false, Visible: true);
                aDoc.Activate();
                FindAndReplace(wordApp, "{idAkte}", textBox14.Text);
                FindAndReplace(wordApp, "{namaBayi}", textBox1.Text);
                FindAndReplace(wordApp, "{wargaNegara}", "INDONESIA");
                FindAndReplace(wordApp, "{alamat}", textBox3.Text);
                FindAndReplace(wordApp, "{date}", detailLahir);
                FindAndReplace(wordApp, "{anakKe}", textBox5.Text);
                FindAndReplace(wordApp, "{kepala}", noKtpKelurahan);
            }
            catch (Exception ex)
            {
                labelResponse.Text = ex.Message;
                labelResponse.BackColor = Color.Pink;
            }
            conn.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                if (textBox8 != null && !string.IsNullOrWhiteSpace(textBox8.Text))
                {
                    cmd.CommandText = "Select * from tabel_ktp where no_ktp = " + textBox8.Text;
                }

                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        noKtpIbu = (reader["no_ktp"].ToString());
                        labelNamaIbu.Text = (reader["nama"].ToString().ToLower());
                        labelTtlIbu.Text = (reader["tempat_tanggal_lahir"].ToString().ToLower());
                        labelPekerjaanIbu.Text = (reader["pekerjaan"].ToString().ToLower());
                        labelAlamatIbu.Text = (reader["alamat"].ToString().ToLower());
                        labelKwnIbu.Text = (reader["kewarganegaraan"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed with error : " + ex.Message);
            }
            conn.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            textBox11.Text = "";
            textBox12.Text = "";
            textBox13.Text = "";
            textBox14.Text = "";

            labelNamaIbu.Text = "-";
            labelTtlIbu.Text = "-";
            labelPekerjaanIbu.Text = "-";
            labelAlamatIbu.Text = "-";
            labelKwnIbu.Text = "-";

            labelNamaPelapor.Text = "-";
            labelNamaSaksi1.Text = "-";
            labelNamaSaksi2.Text = "-";

            labelNamaAyah.Text = "-";
            labelTtlAyah.Text = "-";
            labelPekerjaanAyah.Text = "-";
            labelAlamatAyah.Text = "-";
            labelKwnAyah.Text = "-";

            labelResponse.Text = "Field dibersihkan";
            labelResponse.BackColor = Color.Aqua;
        }

        private void button7_Click_1(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                if (textBox12 != null && !string.IsNullOrWhiteSpace(textBox12.Text))
                {
                    cmd.CommandText = "Select * from tabel_ktp where no_ktp = " + textBox12.Text;
                }

                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        noKtpSaksi2 = (reader["no_ktp"].ToString());
                        labelNamaSaksi2.Text = (reader["nama"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed with error : " + ex.Message);
            }
            conn.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                if (textBox9 != null && !string.IsNullOrWhiteSpace(textBox9.Text))
                {
                    cmd.CommandText = "Select * from tabel_ktp where no_ktp = " + textBox9.Text;
                }

                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        noKtpAyah = (reader["no_ktp"].ToString());
                        labelNamaAyah.Text = (reader["nama"].ToString().ToLower());
                        labelTtlAyah.Text = (reader["tempat_tanggal_lahir"].ToString().ToLower());
                        labelPekerjaanAyah.Text = (reader["pekerjaan"].ToString().ToLower());
                        labelAlamatAyah.Text = (reader["alamat"].ToString().ToLower());
                        labelKwnAyah.Text = (reader["kewarganegaraan"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed with error : " + ex.Message);
            }
            conn.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                if (textBox10 != null && !string.IsNullOrWhiteSpace(textBox10.Text))
                {
                    cmd.CommandText = "Select * from tabel_ktp where no_ktp = " + textBox10.Text;
                }

                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        noKtpPelapor = (reader["no_ktp"].ToString());
                        labelNamaPelapor.Text = (reader["nama"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed with error : " + ex.Message);
            }
            conn.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                if (textBox11 != null && !string.IsNullOrWhiteSpace(textBox11.Text))
                {
                    cmd.CommandText = "Select * from tabel_ktp where no_ktp = " + textBox11.Text;
                }

                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        noKtpSaksi1 = (reader["no_ktp"].ToString());
                        labelNamaSaksi1.Text = (reader["nama"].ToString().ToLower());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed with error : " + ex.Message);
            }
            conn.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                if (textBox13 != null && !string.IsNullOrWhiteSpace(textBox13.Text))
                {
                    cmd.CommandText = "Select * from tabel_ktp where no_ktp = " + textBox13.Text;
                }

                using (IDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        noKtpKelurahan = (reader["no_ktp"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed with error : " + ex.Message);
            }
            conn.Close();
        }

        private void FindAndReplace(Microsoft.Office.Interop.Word.Application doc, object findText, object replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace
            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }
    }
}

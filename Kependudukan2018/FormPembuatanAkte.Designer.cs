﻿namespace Kependudukan2018
{
    partial class FormPembuatanAkte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNama = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.labelPanjang = new System.Windows.Forms.Label();
            this.labelBerat = new System.Windows.Forms.Label();
            this.labelKelahiranKe = new System.Windows.Forms.Label();
            this.labelWaktuLahir = new System.Windows.Forms.Label();
            this.labelTempatKelahiran = new System.Windows.Forms.Label();
            this.labelJenisKelamin = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelKwnIbu = new System.Windows.Forms.Label();
            this.labelAlamatIbu = new System.Windows.Forms.Label();
            this.labelPekerjaanIbu = new System.Windows.Forms.Label();
            this.labelTtlIbu = new System.Windows.Forms.Label();
            this.labelNamaIbu = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.labelKwnAyah = new System.Windows.Forms.Label();
            this.labelAlamatAyah = new System.Windows.Forms.Label();
            this.labelPekerjaanAyah = new System.Windows.Forms.Label();
            this.labelTtlAyah = new System.Windows.Forms.Label();
            this.labelNamaAyah = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.labelNamaPelapor = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.labelNamaSaksi2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.labelNamaSaksi1 = new System.Windows.Forms.Label();
            this.labelResponse = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelNama
            // 
            this.labelNama.AutoSize = true;
            this.labelNama.Location = new System.Drawing.Point(10, 36);
            this.labelNama.Name = "labelNama";
            this.labelNama.Size = new System.Drawing.Size(46, 19);
            this.labelNama.TabIndex = 1;
            this.labelNama.Text = "Nama";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 19);
            this.label2.TabIndex = 8;
            this.label2.Text = "KTP Ibu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "KTP Ayah";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 19);
            this.label5.TabIndex = 11;
            this.label5.Text = "KTP Saksi 1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(568, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 19);
            this.label7.TabIndex = 13;
            this.label7.Text = "KTP Kelurahan";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(162, 36);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(292, 26);
            this.textBox1.TabIndex = 14;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(162, 38);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(233, 26);
            this.textBox8.TabIndex = 21;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(162, 35);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(212, 26);
            this.textBox9.TabIndex = 22;
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.Location = new System.Drawing.Point(678, 31);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(212, 26);
            this.textBox13.TabIndex = 26;
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.Location = new System.Drawing.Point(678, 63);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(212, 26);
            this.textBox14.TabIndex = 29;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(401, 38);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(57, 26);
            this.button3.TabIndex = 31;
            this.button3.Text = "Cari";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(380, 35);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(57, 27);
            this.button4.TabIndex = 32;
            this.button4.Text = "Cari";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(380, 35);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(57, 25);
            this.button5.TabIndex = 33;
            this.button5.Text = "Cari";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(380, 36);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(57, 25);
            this.button6.TabIndex = 34;
            this.button6.Text = "Cari";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(896, 31);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(58, 26);
            this.button8.TabIndex = 36;
            this.button8.Text = "Cari";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(568, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 19);
            this.label8.TabIndex = 37;
            this.label8.Text = "Nomor Akte";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.labelPanjang);
            this.groupBox1.Controls.Add(this.labelBerat);
            this.groupBox1.Controls.Add(this.labelKelahiranKe);
            this.groupBox1.Controls.Add(this.labelWaktuLahir);
            this.groupBox1.Controls.Add(this.labelTempatKelahiran);
            this.groupBox1.Controls.Add(this.labelJenisKelamin);
            this.groupBox1.Controls.Add(this.labelNama);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(23, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(473, 274);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bayi";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(162, 132);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(292, 26);
            this.dateTimePicker1.TabIndex = 36;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(238, 235);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 19);
            this.label11.TabIndex = 35;
            this.label11.Text = "Cm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(238, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 19);
            this.label10.TabIndex = 34;
            this.label10.Text = "Kg";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(204, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 19);
            this.label9.TabIndex = 33;
            this.label9.Text = "L / P";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(162, 228);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(70, 26);
            this.textBox7.TabIndex = 32;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(162, 196);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(70, 26);
            this.textBox6.TabIndex = 31;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(162, 164);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(28, 26);
            this.textBox5.TabIndex = 30;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(162, 100);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(292, 26);
            this.textBox3.TabIndex = 28;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(162, 68);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(28, 26);
            this.textBox2.TabIndex = 27;
            // 
            // labelPanjang
            // 
            this.labelPanjang.AutoSize = true;
            this.labelPanjang.Location = new System.Drawing.Point(10, 228);
            this.labelPanjang.Name = "labelPanjang";
            this.labelPanjang.Size = new System.Drawing.Size(57, 19);
            this.labelPanjang.TabIndex = 26;
            this.labelPanjang.Text = "Panjang";
            // 
            // labelBerat
            // 
            this.labelBerat.AutoSize = true;
            this.labelBerat.Location = new System.Drawing.Point(10, 196);
            this.labelBerat.Name = "labelBerat";
            this.labelBerat.Size = new System.Drawing.Size(42, 19);
            this.labelBerat.TabIndex = 25;
            this.labelBerat.Text = "Berat";
            // 
            // labelKelahiranKe
            // 
            this.labelKelahiranKe.AutoSize = true;
            this.labelKelahiranKe.Location = new System.Drawing.Point(10, 164);
            this.labelKelahiranKe.Name = "labelKelahiranKe";
            this.labelKelahiranKe.Size = new System.Drawing.Size(90, 19);
            this.labelKelahiranKe.TabIndex = 24;
            this.labelKelahiranKe.Text = "Kelahiran Ke";
            // 
            // labelWaktuLahir
            // 
            this.labelWaktuLahir.AutoSize = true;
            this.labelWaktuLahir.Location = new System.Drawing.Point(10, 132);
            this.labelWaktuLahir.Name = "labelWaktuLahir";
            this.labelWaktuLahir.Size = new System.Drawing.Size(123, 19);
            this.labelWaktuLahir.TabIndex = 23;
            this.labelWaktuLahir.Text = "Detail Waktu Lahir";
            // 
            // labelTempatKelahiran
            // 
            this.labelTempatKelahiran.AutoSize = true;
            this.labelTempatKelahiran.Location = new System.Drawing.Point(10, 100);
            this.labelTempatKelahiran.Name = "labelTempatKelahiran";
            this.labelTempatKelahiran.Size = new System.Drawing.Size(116, 19);
            this.labelTempatKelahiran.TabIndex = 22;
            this.labelTempatKelahiran.Text = "Tempat Kelahiran";
            // 
            // labelJenisKelamin
            // 
            this.labelJenisKelamin.AutoSize = true;
            this.labelJenisKelamin.Location = new System.Drawing.Point(10, 68);
            this.labelJenisKelamin.Name = "labelJenisKelamin";
            this.labelJenisKelamin.Size = new System.Drawing.Size(92, 19);
            this.labelJenisKelamin.TabIndex = 21;
            this.labelJenisKelamin.Text = "Jenis Kelamin";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.labelKwnIbu);
            this.groupBox2.Controls.Add(this.labelAlamatIbu);
            this.groupBox2.Controls.Add(this.labelPekerjaanIbu);
            this.groupBox2.Controls.Add(this.labelTtlIbu);
            this.groupBox2.Controls.Add(this.labelNamaIbu);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.textBox8);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(23, 375);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(473, 177);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ibu";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 147);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(119, 19);
            this.label17.TabIndex = 46;
            this.label17.Text = "Kewarganegaraan";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 128);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 19);
            this.label18.TabIndex = 45;
            this.label18.Text = "Alamat";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 109);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 19);
            this.label19.TabIndex = 44;
            this.label19.Text = "Pekerjaan";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 90);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(90, 19);
            this.label20.TabIndex = 43;
            this.label20.Text = "Tanggal Lahir";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(12, 71);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 19);
            this.label21.TabIndex = 42;
            this.label21.Text = "Nama";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(158, 147);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 19);
            this.label12.TabIndex = 41;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(158, 128);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(0, 19);
            this.label13.TabIndex = 40;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(158, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 19);
            this.label14.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(158, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 19);
            this.label15.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(158, 71);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 19);
            this.label16.TabIndex = 37;
            // 
            // labelKwnIbu
            // 
            this.labelKwnIbu.AutoSize = true;
            this.labelKwnIbu.Location = new System.Drawing.Point(158, 147);
            this.labelKwnIbu.Name = "labelKwnIbu";
            this.labelKwnIbu.Size = new System.Drawing.Size(15, 19);
            this.labelKwnIbu.TabIndex = 36;
            this.labelKwnIbu.Text = "-";
            // 
            // labelAlamatIbu
            // 
            this.labelAlamatIbu.AutoSize = true;
            this.labelAlamatIbu.Location = new System.Drawing.Point(158, 128);
            this.labelAlamatIbu.Name = "labelAlamatIbu";
            this.labelAlamatIbu.Size = new System.Drawing.Size(15, 19);
            this.labelAlamatIbu.TabIndex = 35;
            this.labelAlamatIbu.Text = "-";
            // 
            // labelPekerjaanIbu
            // 
            this.labelPekerjaanIbu.AutoSize = true;
            this.labelPekerjaanIbu.Location = new System.Drawing.Point(158, 109);
            this.labelPekerjaanIbu.Name = "labelPekerjaanIbu";
            this.labelPekerjaanIbu.Size = new System.Drawing.Size(15, 19);
            this.labelPekerjaanIbu.TabIndex = 34;
            this.labelPekerjaanIbu.Text = "-";
            // 
            // labelTtlIbu
            // 
            this.labelTtlIbu.AutoSize = true;
            this.labelTtlIbu.Location = new System.Drawing.Point(158, 90);
            this.labelTtlIbu.Name = "labelTtlIbu";
            this.labelTtlIbu.Size = new System.Drawing.Size(15, 19);
            this.labelTtlIbu.TabIndex = 33;
            this.labelTtlIbu.Text = "-";
            // 
            // labelNamaIbu
            // 
            this.labelNamaIbu.AutoSize = true;
            this.labelNamaIbu.Location = new System.Drawing.Point(158, 71);
            this.labelNamaIbu.Name = "labelNamaIbu";
            this.labelNamaIbu.Size = new System.Drawing.Size(15, 19);
            this.labelNamaIbu.TabIndex = 32;
            this.labelNamaIbu.Text = "-";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.labelKwnAyah);
            this.groupBox3.Controls.Add(this.labelAlamatAyah);
            this.groupBox3.Controls.Add(this.labelPekerjaanAyah);
            this.groupBox3.Controls.Add(this.labelTtlAyah);
            this.groupBox3.Controls.Add(this.labelNamaAyah);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.textBox9);
            this.groupBox3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(502, 95);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(454, 184);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ayah";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 147);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(119, 19);
            this.label22.TabIndex = 46;
            this.label22.Text = "Kewarganegaraan";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(11, 128);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 19);
            this.label23.TabIndex = 45;
            this.label23.Text = "Alamat";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(11, 109);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 19);
            this.label24.TabIndex = 44;
            this.label24.Text = "Pekerjaan";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(11, 90);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(90, 19);
            this.label25.TabIndex = 43;
            this.label25.Text = "Tanggal Lahir";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(11, 71);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(46, 19);
            this.label26.TabIndex = 42;
            this.label26.Text = "Nama";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(157, 147);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(0, 19);
            this.label27.TabIndex = 41;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(157, 128);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(0, 19);
            this.label28.TabIndex = 40;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(157, 109);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(0, 19);
            this.label29.TabIndex = 39;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(157, 90);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(0, 19);
            this.label30.TabIndex = 38;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(157, 71);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(0, 19);
            this.label31.TabIndex = 37;
            // 
            // labelKwnAyah
            // 
            this.labelKwnAyah.AutoSize = true;
            this.labelKwnAyah.Location = new System.Drawing.Point(157, 147);
            this.labelKwnAyah.Name = "labelKwnAyah";
            this.labelKwnAyah.Size = new System.Drawing.Size(15, 19);
            this.labelKwnAyah.TabIndex = 36;
            this.labelKwnAyah.Text = "-";
            // 
            // labelAlamatAyah
            // 
            this.labelAlamatAyah.AutoSize = true;
            this.labelAlamatAyah.Location = new System.Drawing.Point(157, 128);
            this.labelAlamatAyah.Name = "labelAlamatAyah";
            this.labelAlamatAyah.Size = new System.Drawing.Size(15, 19);
            this.labelAlamatAyah.TabIndex = 35;
            this.labelAlamatAyah.Text = "-";
            // 
            // labelPekerjaanAyah
            // 
            this.labelPekerjaanAyah.AutoSize = true;
            this.labelPekerjaanAyah.Location = new System.Drawing.Point(157, 109);
            this.labelPekerjaanAyah.Name = "labelPekerjaanAyah";
            this.labelPekerjaanAyah.Size = new System.Drawing.Size(15, 19);
            this.labelPekerjaanAyah.TabIndex = 34;
            this.labelPekerjaanAyah.Text = "-";
            // 
            // labelTtlAyah
            // 
            this.labelTtlAyah.AutoSize = true;
            this.labelTtlAyah.Location = new System.Drawing.Point(157, 90);
            this.labelTtlAyah.Name = "labelTtlAyah";
            this.labelTtlAyah.Size = new System.Drawing.Size(15, 19);
            this.labelTtlAyah.TabIndex = 33;
            this.labelTtlAyah.Text = "-";
            // 
            // labelNamaAyah
            // 
            this.labelNamaAyah.AutoSize = true;
            this.labelNamaAyah.Location = new System.Drawing.Point(157, 71);
            this.labelNamaAyah.Name = "labelNamaAyah";
            this.labelNamaAyah.Size = new System.Drawing.Size(15, 19);
            this.labelNamaAyah.TabIndex = 32;
            this.labelNamaAyah.Text = "-";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox10);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label36);
            this.groupBox4.Controls.Add(this.label37);
            this.groupBox4.Controls.Add(this.label38);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Controls.Add(this.labelNamaPelapor);
            this.groupBox4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(502, 285);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(454, 100);
            this.groupBox4.TabIndex = 48;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Pelapor";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(158, 35);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(216, 26);
            this.textBox10.TabIndex = 44;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 19);
            this.label4.TabIndex = 43;
            this.label4.Text = "KTP Pelapor";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(11, 69);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(46, 19);
            this.label36.TabIndex = 42;
            this.label36.Text = "Nama";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(158, 172);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(0, 19);
            this.label37.TabIndex = 41;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(158, 153);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(0, 19);
            this.label38.TabIndex = 40;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(158, 134);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(0, 19);
            this.label39.TabIndex = 39;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(158, 115);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(0, 19);
            this.label40.TabIndex = 38;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(158, 96);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(0, 19);
            this.label41.TabIndex = 37;
            // 
            // labelNamaPelapor
            // 
            this.labelNamaPelapor.AutoSize = true;
            this.labelNamaPelapor.Location = new System.Drawing.Point(157, 69);
            this.labelNamaPelapor.Name = "labelNamaPelapor";
            this.labelNamaPelapor.Size = new System.Drawing.Size(15, 19);
            this.labelNamaPelapor.TabIndex = 32;
            this.labelNamaPelapor.Text = "-";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.labelNamaSaksi2);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.button7);
            this.groupBox5.Controls.Add(this.textBox12);
            this.groupBox5.Controls.Add(this.textBox11);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.label42);
            this.groupBox5.Controls.Add(this.label43);
            this.groupBox5.Controls.Add(this.button6);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this.labelNamaSaksi1);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(502, 391);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(454, 161);
            this.groupBox5.TabIndex = 49;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Saksi";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(12, 126);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(95, 19);
            this.label32.TabIndex = 50;
            this.label32.Text = "Nama Saksi 2";
            // 
            // labelNamaSaksi2
            // 
            this.labelNamaSaksi2.AutoSize = true;
            this.labelNamaSaksi2.Location = new System.Drawing.Point(154, 126);
            this.labelNamaSaksi2.Name = "labelNamaSaksi2";
            this.labelNamaSaksi2.Size = new System.Drawing.Size(15, 19);
            this.labelNamaSaksi2.TabIndex = 49;
            this.labelNamaSaksi2.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 19);
            this.label6.TabIndex = 48;
            this.label6.Text = "KTP Saksi 2";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(380, 68);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(57, 25);
            this.button7.TabIndex = 47;
            this.button7.Text = "Cari";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click_1);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(158, 67);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(216, 26);
            this.textBox12.TabIndex = 46;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(158, 35);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(216, 26);
            this.textBox11.TabIndex = 45;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(11, 104);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(95, 19);
            this.label33.TabIndex = 42;
            this.label33.Text = "Nama Saksi 1";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(158, 172);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(0, 19);
            this.label34.TabIndex = 41;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(158, 153);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(0, 19);
            this.label35.TabIndex = 40;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(158, 134);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(0, 19);
            this.label42.TabIndex = 39;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(158, 150);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(0, 19);
            this.label43.TabIndex = 38;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(158, 131);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(0, 19);
            this.label44.TabIndex = 37;
            // 
            // labelNamaSaksi1
            // 
            this.labelNamaSaksi1.AutoSize = true;
            this.labelNamaSaksi1.Location = new System.Drawing.Point(154, 104);
            this.labelNamaSaksi1.Name = "labelNamaSaksi1";
            this.labelNamaSaksi1.Size = new System.Drawing.Size(15, 19);
            this.labelNamaSaksi1.TabIndex = 32;
            this.labelNamaSaksi1.Text = "-";
            // 
            // labelResponse
            // 
            this.labelResponse.AutoSize = true;
            this.labelResponse.Location = new System.Drawing.Point(20, 569);
            this.labelResponse.Name = "labelResponse";
            this.labelResponse.Size = new System.Drawing.Size(55, 13);
            this.labelResponse.TabIndex = 50;
            this.labelResponse.Text = "Response";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(870, 558);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 38);
            this.button2.TabIndex = 52;
            this.button2.Text = "Bersihkan";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(768, 558);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 38);
            this.button1.TabIndex = 51;
            this.button1.Text = "Simpan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Rockwell", 16F);
            this.label45.Location = new System.Drawing.Point(46, 25);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(450, 25);
            this.label45.TabIndex = 54;
            this.label45.Text = ".:: FORM PEMBUATAN AKTE KELAHIRAN ::.";
            this.label45.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(78, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(385, 13);
            this.label1.TabIndex = 55;
            this.label1.Text = "_______________________________________________________________";
            // 
            // FormPembuatanAkte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 605);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelResponse);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.label7);
            this.Name = "FormPembuatanAkte";
            this.Text = "FormPembuatanAkte";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelNama;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelJenisKelamin;
        private System.Windows.Forms.Label labelTempatKelahiran;
        private System.Windows.Forms.Label labelWaktuLahir;
        private System.Windows.Forms.Label labelKelahiranKe;
        private System.Windows.Forms.Label labelBerat;
        private System.Windows.Forms.Label labelPanjang;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        public System.Windows.Forms.Label labelKwnIbu;
        public System.Windows.Forms.Label labelAlamatIbu;
        public System.Windows.Forms.Label labelPekerjaanIbu;
        public System.Windows.Forms.Label labelTtlIbu;
        public System.Windows.Forms.Label labelNamaIbu;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.Label label30;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.Label labelKwnAyah;
        public System.Windows.Forms.Label labelAlamatAyah;
        public System.Windows.Forms.Label labelPekerjaanAyah;
        public System.Windows.Forms.Label labelTtlAyah;
        public System.Windows.Forms.Label labelNamaAyah;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.Label label38;
        public System.Windows.Forms.Label label39;
        public System.Windows.Forms.Label label40;
        public System.Windows.Forms.Label label41;
        public System.Windows.Forms.Label labelNamaPelapor;
        private System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label42;
        public System.Windows.Forms.Label label43;
        public System.Windows.Forms.Label label44;
        public System.Windows.Forms.Label labelNamaSaksi1;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label labelResponse;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label labelNamaSaksi2;
        public System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}
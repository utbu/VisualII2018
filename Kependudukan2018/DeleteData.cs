﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Kependudukan2018
{
    public partial class DeleteData : Form
    {
        private static MySqlConnection conn = new MySqlConnection("server =localhost; UID=root; Pwd=; database=kependudukan");
        public DeleteData()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                MySqlCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "DELETE from tabel_ktp WHERE no_ktp='" + textBox1.Text + "'";

                cmd.ExecuteNonQuery();

                label1.Text = "Berhasil Hapus data";
                label1.BackColor = Color.GreenYellow;
            }
            catch (Exception ex)
            {
                label1.Text = ex.Message;
                label1.BackColor = Color.Pink;
            }
            conn.Close();
        }
    }
}

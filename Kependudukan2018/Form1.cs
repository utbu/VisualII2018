﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Kependudukan2018
{
    public partial class Form1 : Form
    {
        private static MySqlConnection conn = new MySqlConnection("server =localhost; UID=root; Pwd=; database=kependudukan");

        public Form1()
        {
            InitializeComponent();
            //Image myimage = new Bitmap(@"C:\Users\ASUS\Documents\Visual Studio 2015\Projects\Kependudukan2018\Kependudukan2018\image_bg.jpg");
            //this.BackgroundImage = myimage;
            //pictureBox1.ImageLocation = @"C:\Users\ASUS\Documents\Visual Studio 2015\Projects\Kependudukan2018\Kependudukan2018\logo.png";

            listView1.Hide();
            tutupDataToolStripMenuItem.Enabled = false;
            tutupDataAkteToolStripMenuItem.Enabled = false;
        }

        private void dataPendudukToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            pictureBox2.Hide();
            label1.Hide();
            label2.Hide();

            listView1.Items.Clear();
            showList();
            listView1.Show();
            dataPendudukToolStripMenuItem.Enabled = false;
            tutupDataToolStripMenuItem.Enabled = true;
        }

        public void showList()
        {

            try
            {
                conn.Open();
                listView1.View = View.Details;
                listView1.Dock = System.Windows.Forms.DockStyle.Fill;
                string query = "Select * from tabel_ktp";
                using (MySqlDataAdapter adapter = new MySqlDataAdapter(query, conn))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    listView1.Columns.Clear();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        listView1.Columns.Add(dc.ColumnName, 50, HorizontalAlignment.Left);
                    }
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        ListViewItem listItem = new ListViewItem(dr["no_ktp"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["nama"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["alamat"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["jenis_kelamin"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["agama"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["status"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["pekerjaan"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["kewarganegaraan"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["tempat_tanggal_lahir"].ToString().ToUpper());

                        listView1.Items.Add(listItem);
                    }

                    ResizeListViewColumns(listView1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed with error : " + ex.Message);
            }

            conn.Close();
        }
        
        // add auto width columns
        private void ResizeListViewColumns(ListView lv)
        {
            foreach (ColumnHeader column in lv.Columns)
            {
                column.AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                column.Width = -2;
            }
        }

        private void tambahDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Input_Data_Baru newForm = new Input_Data_Baru();
            newForm.Show();
        }

        private void editDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Edit_Data formEdit = new Edit_Data();
            formEdit.Show();
        }

        private void tutupDataToolStripMenuItem_Click(object sender, EventArgs e)
        {

            listView1.Hide();
            dataPendudukToolStripMenuItem.Enabled = true;
            tutupDataToolStripMenuItem.Enabled = false;

            pictureBox1.Show();
            pictureBox2.Show();
            label1.Show();
            label2.Show();
        }

        private void hapusDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteData deleteForm = new DeleteData();
            deleteForm.Show();
        }

        private void pembuatanAkteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPembuatanAkte formAkte = new FormPembuatanAkte();
            formAkte.Show();
        }

        private void tampilkanDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            pictureBox2.Hide();
            label1.Hide();
            label2.Hide();

            listView1.Items.Clear();
            showListAkte();
            listView1.Show();
            tampilkanDataToolStripMenuItem.Enabled = false;
            tutupDataAkteToolStripMenuItem.Enabled = true;
        }

        public void showListAkte()
        {

            try
            {
                conn.Open();
                listView1.View = View.Details;
                listView1.Dock = System.Windows.Forms.DockStyle.Fill;
                string query = "Select * from tabel_kelahiran";
                using (MySqlDataAdapter adapter = new MySqlDataAdapter(query, conn))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    listView1.Columns.Clear();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        listView1.Columns.Add(dc.ColumnName, 50, HorizontalAlignment.Left);
                    }
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        ListViewItem listItem = new ListViewItem(dr["id"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["nama"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["jenis_kelamin"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["tempat_lahir"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["waktu_lahir"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["kelahiran_ke"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["berat"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["panjang"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["no_ktp_ibu"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["no_ktp_ayah"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["no_ktp_pelapor"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["no_ktp_saksi_1"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["no_ktp_saksi_2"].ToString().ToUpper());
                        listItem.SubItems.Add(dr["no_ktp_kelurahan"].ToString().ToUpper());

                        listView1.Items.Add(listItem);
                    }
                    ResizeListViewColumns(listView1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed with error : " + ex.Message);
            }

            conn.Close();
        }

        private void tutupDataAkteToolStripMenuItem_Click(object sender, EventArgs e)
        {

            listView1.Show();
            listView1.Hide();
            tampilkanDataToolStripMenuItem.Enabled = true;
            tutupDataAkteToolStripMenuItem.Enabled = false;

            pictureBox1.Show();
            pictureBox2.Show();
            label1.Show();
            label2.Show();
        }

        private void dataKTMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListKTM form = new ListKTM();
            form.Show();
        }

        private void pembuatanKTMToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormKTM form = new FormKTM();
            form.Show();
        }
    }
}
